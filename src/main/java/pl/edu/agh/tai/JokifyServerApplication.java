package pl.edu.agh.tai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class JokifyServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(JokifyServerApplication.class, args);
	}
}
