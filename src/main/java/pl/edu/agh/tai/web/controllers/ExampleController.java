package pl.edu.agh.tai.web.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ExampleController {

    @RequestMapping(value = "/")
    public String helloPage() {
        return "<h1>Jokify</h1>";
    }
}
